#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

struct date {
    int year;
    int month;
    int day;
};

typedef struct user_detail {
    char *name;
    struct date date;
    char sex;
    char *account_type;
    int max_limit;
    size_t id;

    struct user_detail *next;
} user_details;

int print_info(void);
void create_user(user_details**);
void search_user(user_details*);
void ask_user_details(user_details*);
user_details* create_llist(void);
void delete_user(user_details**);
void print_user_details(user_details*);
void create_data_file(user_details*);
user_details read_data_file(user_details**);

int main(void) {
    user_details *user = NULL;
    int input;

    input = print_info();
    printf("%d\n", input);

    switch (input) {
    case 1:
        create_user(&user);
        create_data_file(user);
        break;
    case 2:
        read_data_file(&user);
        search_user(user);
        break;
    case 3:
        delete_user(&user);
    default:
        break;
    }

    print_user_details(user);

    free(user);
    return 0;
}

int print_info(void) {
    int input;

    printf("Bank of CC\n");
    printf("Please use one of the following commands:\n");
    printf("Create User (1), Delete User(2), See User Details (3)\n");
    scanf("%d", &input);

    return input;
}

void create_user(user_details **user) {
    user_details *p = NULL;
    size_t user_id = 0;
    int stop = 0;

    do {
        user_details *temp = create_llist();
        temp->id = user_id;

        ask_user_details(temp);

        if(*user == NULL) {
            *user = temp;
        } else {
            p = *user;
            while(p->next != NULL) {
                p = p->next;
            }
            p->next = temp;
        }
        user_id++;

        printf("Stop/Continue (0/1): ");
        scanf("%d", &stop);
    } while(stop == 1);
}

void ask_user_details(user_details *temp) {
    char *name1, *name2;
    name1 = (char*)malloc(sizeof(char) * 20);
    name2 = (char*)malloc(sizeof(char) * 20);

    printf("Enter Name (FirstName LastName): ");
    scanf("%s %s", name1, name2);
    strcat(name1, " ");
    strcat(name1, name2);
    strcpy(temp->name, name1);

    free(name1);
    free(name2);

    printf("Enter date of birth (DD): ");
    scanf("%d", &(temp->date.day));
    printf("(MM): ");
    scanf("%d", &(temp->date.month));
    printf("(YYYY): ");
    scanf("%d", &(temp->date.year));

    printf("Enter Sex (M/F): ");
    scanf(" %c", &(temp->sex));
    temp->sex = toupper(temp->sex);

    printf("Enter Account Type: ");
    scanf("%s", temp->account_type);

    printf("Enter Account Limit ($1000): ");
    scanf("%d", &(temp->max_limit));
}

// Searches the user that I specify
void search_user(user_details *user) {
    user_details *current = user; 
    int user_id = 0;

    printf("Search Data about USER ID (x): ");
    scanf("%d", &user_id);
    while(current != NULL) { 
        if(current->id == user_id) {
            printf("-----------------\n");
            printf("%s\n", current->name);
            printf("%s\n", current->account_type);
            printf("%d\n", current->date);
            printf("%d\n", current->max_limit);
            printf("%c\n", current->sex);
            printf("%d\n", current->id);
            printf("-----------------\n");
        }
        current = current->next; 
    } 
}

void print_user_details(user_details *user) {
    user_details *p = user;

    while(p != NULL) {
        printf("User Name: %s\n", p->name);
        printf("User date of birth: %.2d/%.2d/%.4d\n", p->date.day, p->date.month, p->date.year);
        printf("User Sex: %c\n", p->sex);
        printf("Usr Account Type: %s\n", p->account_type);
        printf("User Account Maximum Limit: $%d\n", p->max_limit);
        printf("User ID: %d\n", p->id);

        p = p->next;
    }
}

user_details* create_llist() {
    user_details *temp = (user_details*)malloc(sizeof(user_details));
    temp->next = NULL;
    temp->name = (char*)malloc(sizeof(char*) * 50);
    temp->account_type = (char*)malloc(sizeof(char*) * 20);

    return temp;
}

void delete_user(user_details **user) {
    if(*user == NULL) return;

    user_details *temp = *user;
    
    if(temp == 0) {
        *user = temp->next;
        free(temp);
        return;
    }

    size_t user_id = 1000;
    printf("Please enter the USER ID of the account you would like to delete (0): ");
    scanf("%d", &user_id);

    for(size_t i = 0; temp != NULL && i < user_id - 1; i++) {
        temp = temp->next;
    }

    if(temp == NULL || temp->next == NULL) return;

    user_details *next = temp->next->next;

    free(temp->next);

    temp->next = next;

    printf("Deleted user id: %d\n", user_id);
}

void create_data_file(user_details *user) {
    size_t amount_of_users = user->id + 1;
    char *file_name = "users.dat";
    FILE *outfile;
    user_details *iterator = user;

    outfile = fopen(file_name, "wb");
    if(outfile == NULL) {
        fprintf(stderr, "\nError opening file\n");
        exit(1);
    }

    /*
    while(fwrite(user->name, sizeof(char), strlen(user->name) + 1, outfile) &&
    fwrite(user->account_type, sizeof(char), strlen(user->account_type) + 1, outfile) &&
    user->name != NULL && user->account_type != NULL);
    fwrite(&(user->sex), sizeof(user->name), 1, outfile);
    fwrite(&(user->date.day), sizeof(int), 2, outfile);
    */

    while(iterator != NULL) {
        fwrite(user->name, sizeof(char), strlen(user->name) + 1, outfile); 
        fwrite(user->account_type, sizeof(char), strlen(user->account_type) + 1, outfile);
        //fprintf(fptr, "\nPet Name: %s\nAge: %d\n", iterator->name, iterator->age);
        iterator= iterator->next;
    }

    if(fwrite != 0) {
        printf("File %s created and user data saved\n", file_name);
    } else {
        printf("Error writing file\n");
    }

    fclose(outfile);
}

user_details read_data_file(user_details **user) {
    FILE *infile;

    infile = fopen("/home/camilo/Documents/bank_database/users.dat", "rb");
    if(infile == NULL) {
        fprintf(stderr, "\nError opening file\n");
        exit(1);
    }

    *user = create_llist();
    void *buf = malloc(1024);

    while(true) {
        fread((*user)->name, sizeof(char), 13, infile);
        /*
        printf("%d\n", input->max_limit);
        printf("%c\n", input->sex);
        printf("%.2d/%.2d/%.4d\n", input->date.day, input->date.month, input->date.year);
        */
       printf("Name :%s\n", (*user)->name);

       if((*user)->name == "\0") {
           break;
       }
    }

    while(fread((*user)->account_type, sizeof(char), 8, infile))  {
        printf("Account Type: %s\n", (*user)->account_type);
    }

    /*
    while(fread(buf, sizeof(char), 1, infile))  {
        (*user)->sex = (char)*buf;
        printf("%c\n", (*user)->sex);
    }

    while(fread(buf, sizeof(int), 2, infile)) {
        (*user)->date.day = (int)*buf;
        printf("%.2d\n", (*user)->date.day);
    }
    */
}